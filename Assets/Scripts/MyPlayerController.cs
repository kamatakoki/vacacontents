﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPlayerController : MonoBehaviour
{
    //コントローラー
    OVRInput.Controller RController;
    OVRInput.Controller LController;
    Rigidbody rb;
    //ベクター3型で加速度をとる変数
    Vector3 RAcceration;
    Vector3 LAcceration;

    //動く最低値これより仮想度が大きくならないと動かない
    float Walkspeed = 10;
    //右のコントローラーの加速度と左手のコントローラーの加速度を掛け合わせる(y軸)
    float movespeed;
    //スピード調節
    public float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
        RController = OVRInput.Controller.RTrackedRemote;
        LController = OVRInput.Controller.LTrackedRemote;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        RAcceration = OVRInput.GetLocalControllerAcceleration(RController);
        LAcceration = OVRInput.GetLocalControllerAcceleration(LController);
        movespeed = Mathf.Abs(RAcceration.y) + Mathf.Abs(LAcceration.y);

        if (movespeed >= Walkspeed)
        {
            var moveDirect = transform.rotation.eulerAngles.y;
            var moveQuate = Quaternion.Euler(0, moveDirect, 0);
            transform.position += (moveQuate * Vector3.forward * movespeed * movespeed * movespeed) .normalized * speed *Time.deltaTime;
        }
    }
}
