﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class stopScript : MonoBehaviour
{
    public bool start = true;
    public bool trafficA;
    carsmove cm;
    // Start is called before the first frame update
    void Start()
    {
        cm = GetComponentInParent<carsmove>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Car" || other.tag == "Side")
        {
            start = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Car" || other.tag == "Side")
        {
            start = true;
        }
    }
}
