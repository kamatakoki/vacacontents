﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class goalscript : MonoBehaviour
{
    [SerializeField]
    GameObject camera1;
    public  PlayableDirector PD;
    public int count = 0;
    AudioSource au;
    [SerializeField]
    OVRScreenFade OVRSF;
    // Start is called before the first frame update
    void Start()
    {
        au = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Goal")
        {
            PD.Play();
            this.gameObject.SetActive(false);
            camera1.SetActive(true);
        }
        if(other.tag == "body")
        {
            count++;
            au.Play();
            OVRSF.ShoutotuFadeIn();
        }
    }
}
