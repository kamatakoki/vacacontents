﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class StartSceneAnimChange : MonoBehaviour
{
    [SerializeField]
    PlayableDirector PD;
    public AsyncOperation Async;

    //タイムラインを止める時間
    float stoptimeline = 0;

    //プレイヤーの装備
    [SerializeField]
    GameObject Player;
    [SerializeField]
    GameObject RController;
    [SerializeField]
    GameObject LController;
    MyPlayerController mp;

    //フェードイン、フェードアウト
    [SerializeField]
    OVRScreenFade OVRSF;


    // Start is called before the first frame update
    void Start()
    {
        LoadScene();
        PD.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        stoptimeline += Time.deltaTime;
        if (OVRInput.GetDown(OVRInput.RawButton.A))
        {
            PD.Play(); 
        }
        if(stoptimeline >= 102)
        {
            PD.Pause();
        }
    }

    void MovingPlayer()
    {
        Player.transform.localPosition = new Vector3(-7.1f, 1.5f, 3.85f);
        Player.transform.rotation = Quaternion.Euler(0, 270, 0);
        RController.SetActive(true);
        LController.SetActive(true);
        mp.enabled = true;
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "goal")
        {
            PD.Play();
        }
    }

    private void LoadScene()
    {
        Async = SceneManager.LoadSceneAsync("DefaultScene");
        Async.allowSceneActivation = false;
    }

    IEnumerator SceneChange()
    {
        while (true)
        {
            yield return null;
            if (Async.progress >= 0.9f)
            {
                Async.allowSceneActivation = true;
                break;
            }
        }
    }
    // フェードアウト
    void FadeOut()
    {
        OVRSF.FadeOut();
    }

}
