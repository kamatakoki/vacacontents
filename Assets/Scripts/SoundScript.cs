﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour
{
    [SerializeField]
    AudioSource Horn;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RandomHorn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator RandomHorn()
    {
        while (true)
        {
            Horn.Play();
            yield return new WaitForSeconds(1f);
            Horn.Play(); yield return new WaitForSeconds(5f);
            Horn.Play();
            yield return new WaitForSeconds(0.1f);
            Horn.Play();
            yield return new WaitForSeconds(3f);
            Horn.Play();
        }
    }
}
