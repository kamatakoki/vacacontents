﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ThirdSceneNavigationAI : MonoBehaviour
{
    //canvas
    [SerializeField]
    TextMeshProUGUI sec;
    [SerializeField]
    TextMeshProUGUI num;
    [SerializeField]
    goalscript gs;

    //前回の時の記録]
    [SerializeField]
    TextMeshProUGUI Lnum;
    [SerializeField]
    TextMeshProUGUI Lsec;
    //時間計測
    float tit = 0;
    bool onTimer = false;

    //フェードアウト
    [SerializeField]
    OVRScreenFade OVRSF;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (onTimer)
        {
            tit += Time.deltaTime;
        }
    }

    void textChange()
    {
        num.text = gs.count.ToString() + "回";
        sec.text = Mathf.Round(tit).ToString() + "秒";

        Lsec.text = LogScore.Sec.ToString() + "秒";
        Lnum.text = LogScore.Num.ToString() + "回";
    }

    void FadeOut()
    {
        OVRSF.FadeOut();
    }

    void ReStart()
    {
        SceneManager.LoadSceneAsync("StartScene");
    }


    void Ontimer()
    {
        onTimer = true;
    }
}
