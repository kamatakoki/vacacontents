﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SecondSceneNavigationAI : MonoBehaviour
{
    [SerializeField]
    goalscript gs;
    //時間計測
    float tit = 0;
    bool onTimer = false;

    //記録保持
    float Lsec;
    int Lnum;

    //ロードシーン
    AsyncOperation Async;

    //フェードアウト
    [SerializeField]
    OVRScreenFade OVRSF;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (onTimer)
        {
            tit += Time.deltaTime;
        }
    }

    void Ontimer()
    {
        onTimer = true;
    }

    void textChange()
    {
        Lsec = Mathf.Round(tit);
        Lnum = gs.count;

        LogScore.Num = Lnum;
        LogScore.Sec = Lsec;

        LoadScene();
    }

    IEnumerator ChangeScene()
    {
        yield return null;
        if(Async.progress >= 0.9f)
        {
            Async.allowSceneActivation = true;
        }
    }

    void FadeOut()
    {
        OVRSF.FadeOut();
    }

    void LoadScene()
    {
        Async = SceneManager.LoadSceneAsync("citysample");
        Async.allowSceneActivation = false;
    }
}
