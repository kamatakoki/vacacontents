﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trafficmanager : MonoBehaviour
{
    public Material TrafficGreenA;
    public Material TrafficYellowA;
    public Material TrafficRedA;

    public Material TrafficGreenB;
    public Material TrafficYellowB;
    public Material TrafficRedB;

    public Material TrafficWalkGreenA;
    public Material TrafficWalkRedA;

    public float TrafficChanges = 70;
    public bool trafficAon = true;
    public bool trafficBon = false;

    //アニメーターでストップの制御
    [SerializeField]
    Animator Curve1Stop;
    [SerializeField]
    Animator Curve2Stop;
    [SerializeField]
    Animator Curve3Stop;
    [SerializeField]
    Animator Curve4Stop;
    // Start is called before the first frame update
    void Start()
    {
        TrafficGreenA.EnableKeyword("_EMISSION");
        TrafficYellowA.DisableKeyword("_EMISSION");
        TrafficRedA.DisableKeyword("_EMISSION");
        TrafficGreenB.DisableKeyword("_EMISSION");
        TrafficYellowB.DisableKeyword("_EMISSION");
        TrafficRedB.EnableKeyword("_EMISSION");
        TrafficWalkGreenA.DisableKeyword("_EMISSION");
        TrafficWalkRedA.EnableKeyword("_EMISSION");


        Curve1Stop.SetBool("isStop", false);
        Curve2Stop.SetBool("isStop", false);
        Curve3Stop.SetBool("isStop", true);
        Curve4Stop.SetBool("isStop", true);

    }

    // Update is called once per frame
    void Update()
    {
        TrafficChanges -= Time.deltaTime;
        // 信号A青信号
        //信号A黄色信号
        if(TrafficChanges <= 55)
        {
            TrafficGreenA.DisableKeyword("_EMISSION");
            TrafficYellowA.EnableKeyword("_EMISSION");
        }
        //信号A赤信号, 信号B青信号
        else if(TrafficChanges <= 50)
        {

            TrafficYellowA.DisableKeyword("_EMISSION");
            TrafficRedA.EnableKeyword("_EMISSION");

            TrafficGreenB.EnableKeyword("_EMISSION");
            TrafficRedB.DisableKeyword("_EMISSION");

            trafficAon = false;
            trafficBon = true;

            Curve1Stop.SetBool("isStop", true);
            Curve2Stop.SetBool("isStop", true);
            Curve3Stop.SetBool("isStop", false);
            Curve4Stop.SetBool("isStop", false);
        }
        //信号B黄色信号
        else if(TrafficChanges <= 35)
        {
            TrafficGreenB.DisableKeyword("_EMISSION");
            TrafficYellowB.EnableKeyword("_EMISSION");
        }
        //信号B赤信号
        //信号Aも赤信号
        //歩行者青信号
        else if(TrafficChanges <= 30)
        {
            TrafficYellowB.DisableKeyword("_EMISSION");
            TrafficRedB.EnableKeyword("_EMISSION");

            TrafficWalkGreenA.EnableKeyword("_EMISSION");
            TrafficWalkRedA.DisableKeyword("_EMISSION");
            trafficBon = false;

            Curve3Stop.SetBool("isStop", true);
            Curve4Stop.SetBool("isStop", true);

        }
        //歩行者青信号
        //信号A赤信号
        else if(TrafficChanges <= 0)
        {
            TrafficWalkGreenA.DisableKeyword("_EMISSION");
            TrafficWalkRedA.EnableKeyword("_EMISSION");
            TrafficGreenA.EnableKeyword("_EMISSION");
            TrafficRedA.DisableKeyword("_EMISSION");
            TrafficChanges = 70;

            trafficAon = true;

            Curve1Stop.SetBool("isStop", false);
            Curve2Stop.SetBool("isStop", false);

        }
    }
}
