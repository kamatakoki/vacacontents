﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class carsmanager : MonoBehaviour
{
    float instancetimer = 0;
    public GameObject Car1;
    public GameObject CarofTurn1;
    public GameObject CarofTurn2;
    public GameObject CarofTurn3;
    public GameObject CarofTurn4;

    int count = 0;
    [SerializeField]
    Transform GoStraight1;
    [SerializeField]
    Transform GoStraight2;
    [SerializeField]
    Transform GoStraight3;
    [SerializeField]
    Transform GoStraight4;

    public int matCount = 0;
  
    public Material[] Materials = new Material[5];

    [SerializeField]
    float GenerateTime = 0.8f;
    [SerializeField]
    int MaxGenerateCount = 5;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CarGenerator());
    }

    // Update is called once per frame
    void Update()
    {
    }


    IEnumerator CarGenerator()
    {
        while(count <= MaxGenerateCount)
        {
            if (CarofTurn1 != null && CarofTurn2 != null && CarofTurn3 != null && CarofTurn4 != null)
            {
                Instantiate(CarofTurn1);
                Instantiate(CarofTurn2);
                Instantiate(CarofTurn3);
                Instantiate(CarofTurn4);
            }
            Instantiate(Car1, GoStraight1.position, GoStraight1.rotation);
            yield return new WaitForSeconds(GenerateTime);
            Instantiate(Car1, GoStraight2.position, GoStraight2.rotation);
            yield return new WaitForSeconds(GenerateTime + 0.1f);
            Instantiate(Car1, GoStraight3.position, GoStraight3.rotation);
            yield return new WaitForSeconds(GenerateTime + 0.2f);
            Instantiate(Car1, GoStraight4.position, GoStraight4.rotation);
            yield return new WaitForSeconds(GenerateTime + 0.3f);

            count++;
            matCount++;
        }
    }
}
