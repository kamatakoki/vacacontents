﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class carsmove : MonoBehaviour
{
    public Rigidbody rb;
    //スピード調節
    [SerializeField]
    float speed = 20;
    //減速スピード
    float speedmator = 0.1f;
    stopScript stopS;

    //ゲームオブジェクトのスタート位置
    public Vector3 startPosition;
    public Quaternion startRotation; 

    //最初の位置の判定
    bool StartRight = false;
    bool StartLeft = false;
    bool StartUp = false;
    bool StartDown = false;

    //sideコライダー
    [SerializeField]
    GameObject side;

    public trafficmanager TM;
    public carsmanager cm;
    bool ColiTraffic = false;
    //上下だった場合true
    bool carUD = true;

    [SerializeField]
    Vector3 Myvelocity;
    //velocityの減算の下限
    Vector3 minimumV = new Vector3(0.0001f, 0.0001f, 0.0001f);

    //trueやfalseになった瞬間だけ起動させるためのカウント
    int TFcount = 0;

    public Vector3 ColiUP = new Vector3(0, 1, 0);
    Vector3 SideColistartPosition;
    Renderer Trenderer;
    // Start is called before the first frame update

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        Trenderer = GetComponent<Renderer>();
        cm = GameObject.Find("CarsManager").GetComponent<carsmanager>();
    }
    void Start()
    {
        startPosition = rb.position;
        startRotation = rb.rotation;
        SideColistartPosition = side.transform.localPosition;

        stopS = GetComponentInChildren<stopScript>();
        TM = GameObject.Find("TrafficOnOff").GetComponent<trafficmanager>();

        //最初の位置識別
        if (startPosition.x > 100) StartRight = true;
        else if (startPosition.x < -100) StartLeft = true;
        else if (startPosition.z < -100) StartUp = true;
        else if (startPosition.z > 100) StartDown = true;

        //片方だけ、サイドのコライダー消す
        if (StartRight || StartLeft)
        {
            side.GetComponent<Collider>().enabled = false;
            carUD = false;
        }
        rb.freezeRotation = true;

        if (StartRight) Trenderer.sharedMaterial = cm.Materials[Mathf.Abs(5 - cm.matCount)];
        else if (StartLeft) Trenderer.sharedMaterial = cm.Materials[Mathf.Abs(4 - cm.matCount)];
        else if (StartUp) Trenderer.sharedMaterial = cm.Materials[Mathf.Abs(3 - cm.matCount)];
        else if (StartDown) Trenderer.sharedMaterial = cm.Materials[Mathf.Abs(2 - cm.matCount)];
    }

    // Update is called once per frame
    void Update()
    {
        if (speedmator >= 0)
        {
            speedmator -= 0.000008f;
        }
        speed = 100;
        if (stopS.start)
        {
            if (TFcount == 0) rb.velocity = Vector3.zero;
            if (rb.velocity.magnitude <= 16)
            {
                rb.AddForce(transform.right * Time.deltaTime * 100 * speed);
            }
            TFcount++;
            if (carUD)
            {
                side.transform.position = SideColistartPosition + transform.position;
            }
        }
        else if (stopS.start == false)
        {
            if (rb.velocity.magnitude >= minimumV.magnitude) rb.velocity *= 0.92f;
            TFcount = 0;
            if (carUD)
            {
                side.transform.position += ColiUP;
            }
        }

        Myvelocity = rb.velocity;
    }

    private void OnTriggerStay(Collider other)
    {
        {
            if (other.tag == "bill")
            {
                transform.position = startPosition;
                transform.rotation = startRotation;
            }

            if (SceneManager.GetActiveScene().name == "DefaultScene" && other.tag == "traffic")
            {
                TrafficStop();
            }
        }
    }


    public void TrafficStop()
    {
        if(StartRight || StartLeft)
        {
            if (TM.trafficAon)
            {
                stopS.start = true;
            }
            if(!TM.trafficAon)
            {
                stopS.start = false;
            }
        }
        else if((StartUp || StartDown))
        {
            if (TM.trafficBon)
            {
                stopS.start = true;
            }
            if(!TM.trafficBon)
            {
                stopS.start = false;
            }
        }
    }
}
